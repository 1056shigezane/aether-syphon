//
//  ReciverView.swift
//  Vapor Syphon
//
//  Created by 稲田成実 on 2018/07/24.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

import Foundation
import Cocoa
import Aether
import CoreVideo
import CoreMedia



class VapeurSyphonClient : NSObject, VideoClientDelegate {
    
    var name : String = ""
    
    var server : NetService!
    
    
    var vaporClient : VideoClient? = nil
    var syphonServer : SyphonServer? = nil
    
    var textureCache : CVTextureCache!
    
    var previewWindow : NSWindow!
    var previewController : VaporClientPreviewController!
    
    
    init(withService: NetService) {
        super.init()
        server = withService
        name = (server?.name)!
        previewController = NSStoryboard.init(name: "Main", bundle: Bundle.main).instantiateController(withIdentifier: "VaporClientPreviewController") as? VaporClientPreviewController
        
        previewController.loadView()
        previewController.viewDidLoad()
        
        previewWindow = NSWindow(contentViewController: previewController)
        previewWindow.titlebarAppearsTransparent = true
        previewWindow.styleMask = NSWindow.StyleMask(rawValue: previewWindow.styleMask.rawValue | NSWindow.StyleMask.fullSizeContentView.rawValue)
        
        textureCache = CVTextureCache(with: previewController.openGLView.openGLContext!)
    }
    
    deinit{
        print("VapeurSyphonClient deinit")
    }
    
    
    var request : Bool = false {
        didSet {
            
            if self.request != oldValue {
                if self.request {
                    if let cglctx = self.previewController.openGLView.openGLContext?.cglContextObj {
                       
                       self.vaporClient = VideoClient.init(service: self.server)
                        self.vaporClient!.delegate = self
                        
                        
                        
                        if self.syphonServer == nil {
                            let str = self.vaporClient?.serverName
                            self.syphonServer = SyphonServer.init(name: str, context: cglctx, options: nil)
                        }
                        
                        self.previewWindow.title = "Client: - " + self.server.name
                        self.previewWindow.orderFrontRegardless()
                    }
                    
                }else{
                    
                    if self.syphonServer != nil {
                        self.syphonServer?.stop()
                        self.syphonServer = nil
                    }
                    
                    self.vaporClient?.stop()
                    self.vaporClient?.delegate = nil
                    self.vaporClient = nil
                    
                    previewController.openGLView.image = nil
                    self.previewWindow.close()
                }
            }
            //}
        }
    }
    
    func serverRetired(client tmp:VideoClient) {
        print("VapeurSyphonClient serverRetired")
        
        vaporClient?.stop()
        vaporClient = nil
        request = false
        previewWindow.close()
        
        syphonServer?.stop()
       
        syphonServer = nil
        
        previewController.openGLView.image = nil
        previewWindow.contentViewController = nil
        
    }
    
    
    
    
    func newFrameCallBack(imageBuffer : CVImageBuffer) {
        
        let width = CVPixelBufferGetWidth(imageBuffer)
        let height = CVPixelBufferGetHeight(imageBuffer)
        self.textureCache.cacheToTexture(pixelBuffer: imageBuffer, width: width, height: height)
        
        let name = CVOpenGLTextureGetName(self.textureCache.cvOpenGLTexture!)
        let tgt = CVOpenGLTextureGetTarget(self.textureCache.cvOpenGLTexture!)
        
        
        if self.previewController.isPresenting {
            
            
            CVPixelBufferLockBaseAddress(imageBuffer, .readOnly)
            self.previewController.openGLView.image = (name, NSSize(width: width, height: height),target : tgt)
            CVPixelBufferUnlockBaseAddress(imageBuffer, .readOnly)
        }
        
        
        if let ctx = self.syphonServer?.context {
            self.syphonServer?.publishFrameTexture(name, textureTarget: tgt, imageRegion: NSRect(x: 0, y: 0, width: width, height: height), textureDimensions: NSSize(width: width, height: height), flipped: true)
        }
        
        
        
        
        
    }
    
    func preview(){
        previewWindow.orderFrontRegardless()
    }
    
}

class RecieverView : NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    
    @IBOutlet weak var tableView: NSTableView!
    var clients : [NetService:VapeurSyphonClient] = [:]
    
    override func viewDidLoad() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(vprDirectoryDidChange), name: ServerDirectory.serverDirectoryDidChangeNotificationName, object: nil)
        vprDirectoryDidChange()
        
        
        
    }
    
    
    @objc func vprDirectoryDidChange() {
        
        let servers = ServerDirectory.shared.foundServices // as! [[String:Any]]
        
        for server in servers {
            
            if clients.index(forKey: server) == nil { // key has empty
                clients[server] = VapeurSyphonClient.init(withService: server)
            }
        }
        
        for key in clients.keys {
            
                
                if !(ServerDirectory.shared.foundServices.contains(key)) {//!contains
                    
                    clients[key]?.request = false
                    clients.removeValue(forKey: key)
                    
                }
                
                
            
        }
        
        
        tableView.reloadData()
    }
    
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        
        return clients.count
        
    }
    
    func tableView(_ tableView: NSTableView, sizeToFitWidthOfColumn column: Int) -> CGFloat {
        return tableView.frame.width
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        
        let view = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ReceiverCellView"), owner: self) as! ReceiverCellView
        
        view.vapeurSyphonClient = clients[ServerDirectory.shared.foundServices[row]]
        
        
        
        return view
    }
    
    override func viewWillAppear() {
        view.needsLayout = true
    }
    
    override func viewDidAppear() {
        view.needsLayout = true
        tableView.sizeLastColumnToFit()
    }
}

class ReceiverCellView : NSView {
    
    weak var vapeurSyphonClient : VapeurSyphonClient! {
        didSet{
            name.stringValue = vapeurSyphonClient.name
            requestToggle.state = vapeurSyphonClient.request ? .on : .off
        }
    }
   
    
    @IBAction func previewButtonPressed(_ sender: Any) {
        vapeurSyphonClient.preview()
    }
    @IBOutlet weak var name: NSTextField!
    @IBOutlet weak var requestToggle: NSButton!
    @IBAction func requestToggleChanged(_ sender: NSButton) {
        vapeurSyphonClient.request = sender.state == .on
    }
    
    deinit {
        print("ReceiverCellView deinit")
    }
    
    
    
    
}

class VaporClientPreviewController : NSViewController {
    @IBOutlet weak var openGLView: SyphonReceiverView!
    var isPresenting : Bool = false
    deinit {
        print("VaporClientPreviewController deinit")
    }
    
    override func viewDidAppear() {
        isPresenting = true
    }
    
    override func viewDidDisappear() {
        isPresenting = false
    }
}

extension NSWindow {
    var titlebarHeight: CGFloat {
        let contentHeight = contentRect(forFrameRect: frame).height
        return frame.height - contentHeight
    }
}


class SyphonReceiverView : NSOpenGLView {
    
    typealias Image = (textureName:GLuint,textureSize:NSSize, target : GLuint)
    var image : Image? = nil {
        didSet{
            self.draw(frame)
        }
    }
    
    deinit{
        print("SyphonReceiverView deinit")
        image = nil
        CGLDestroyContext((openGLContext?.cglContextObj)!)
        openGLContext = nil
    }
    
    
    
    
    //var frameSize : NSSize = .zero
    var needsReshape : Bool = false
    
    
    override func draw(_ dirtyRect: NSRect) {
        
       
   //     print("Sender Cache Thread is Main:", Thread.current == .main)
        
        
        if let image = image {
            openGLContext?.makeCurrentContext()
            
            let textureSize = image.textureSize
            let frameSize = self.frame.size
            //print("image==image")
            
          /*  print("ContentScaleFactor",dirtyRect)
            print("FSIZE",frameSize)
            print("TSIZE",textureSize)*/
            
            //frameSize = self.renderSize;
            
            //self.needsReshape = frameSize != rSize
           // frameSize = rSize
            
           // if (self.needsReshape){
                // Setup OpenGL states
            glViewport(0, 0, GLsizei(frameSize.width * 2), GLsizei(frameSize.height * 2));
            
            glMatrixMode(GLenum(GL_PROJECTION));
            glLoadIdentity();
            glOrtho(0.0, GLdouble(frameSize.width), GLdouble(frameSize.height), 0.0, -1, 1);
            
            glMatrixMode(GLenum(GL_MODELVIEW));
            glLoadIdentity();
            
           // glTranslated(GLdouble(frameSize.width * 0.5), GLdouble(frameSize.height * 0.5), 0.0);
            
            
           // }
            
            
            
            glClearColor(0.0, 0.0, 1.0, 1.0);
            glClear(GLbitfield(GL_COLOR_BUFFER_BIT));
            
            //if let image = self.image {
            
            
            
            glEnable(GLenum(image.target))
            
            
            glBindTexture(GLenum(image.target), image.textureName)
            
            
            
            glColor4f(1.0, 1.0, 1.0, 1.0);
            
            
            
            let tex_coords : [GLfloat] =
                [
                    0.0,                0.0,
                    Float(textureSize.width),  0.0,
                    Float(textureSize.width),  Float(textureSize.height),
                    0.0,                Float(textureSize.height)
            ]
            
           
            let verts : [GLfloat] =
                [
                    0, 0,
                    Float(frameSize.width), 0,
                    Float(frameSize.width), Float(frameSize.height),
                    0, Float(frameSize.height)
            ]
            
            glEnableClientState( GLenum(GL_TEXTURE_COORD_ARRAY) )
            glTexCoordPointer(2, GLenum(GL_FLOAT), 0, tex_coords )
            glEnableClientState(GLenum(GL_VERTEX_ARRAY))
            glVertexPointer(2, GLenum(GL_FLOAT), 0, verts )
            glDrawArrays( GLenum(GL_TRIANGLE_FAN), 0, 4 )
            glDisableClientState(GLenum(GL_VERTEX_ARRAY))
            glDisableClientState( GLenum(GL_TEXTURE_COORD_ARRAY) )
            
            
            glBindTexture(GLenum(image.target), 0)
            glDisable(GLenum(image.target))
            
            
           // openGLContext?.flushBuffer()
            glFlush()
            
        }
    }
    
    
    
}
