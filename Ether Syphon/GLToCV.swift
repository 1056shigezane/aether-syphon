//
//  GLToCV.swift
//  Vapor Syphon
//
//  Created by 稲田成実 on 2018/07/24.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

import Foundation
import GLKit
import GLUT
import Metal
import CoreVideo



class GLToCV : NSObject {
    
    var context : NSOpenGLContext!
    var cvPixelBuffer : CVPixelBuffer? = nil
    var cvTexture : CVOpenGLTexture? = nil
    
    var depthBuffer : GLuint = 0
    var frameBuffer : GLuint = 0
    
    //var client : SyphonClient!
    var textureCache : CVOpenGLTextureCache? = nil
    var processSize : NSSize = NSSize.zero
    
    func createCVBuffer(size:(width:Int, height:Int), tclient : SyphonClient) {
        
        
        
        guard let ctx = tclient.context, let fmt = CGLGetPixelFormat(ctx), let glfmt = NSOpenGLPixelFormat.init(cglPixelFormatObj: fmt)  else {
            print("ctx,fmt,glfmt,create Failed")
            return
        }
        
        context = NSOpenGLContext.init (format: glfmt, share: nil)
        
        
        
        guard let cglctx = context.cglContextObj else{
            print("cglCtx is nil")
            return
        }
        
        let cvret = CVOpenGLTextureCacheCreate(kCFAllocatorDefault, nil, cglctx, fmt, nil, &textureCache)
        /*var keycb = kCFTypeDictionaryKeyCallBacks
        var valuecb = kCFTypeDictionaryValueCallBacks
        let empty = CFDictionaryCreate(kCFAllocatorDefault, // our empty IOSurface properties dictionary
            nil,
            nil,
            0,
            &keycb,
            &valuecb);
        let attrs = CFDictionaryCreateMutable(kCFAllocatorDefault, 1,
                                          &keycb,
                                          &valuecb);
        var propKey = kCVPixelBufferIOSurfacePropertiesKey
        CFDictionarySetValue(attrs, &propKey, empty);
        */
        
        /*let options = [
            kCVPixelBufferIOSurfacePropertiesKey as String: [kCFTypeDictionaryKeyCallBacks,kCFTypeDictionaryValueCallBacks],
        ]*/
        
        
        CVPixelBufferCreate(kCFAllocatorDefault, size.width, size.height, kCVPixelFormatType_32BGRA, nil, &cvPixelBuffer)
        
        
        
        guard textureCache != nil, cvPixelBuffer != nil else{
            print("textureCache, cvPixelBuffer is nil")
            return
        }
        
        
        processSize = NSSize(width: size.width, height: size.height)
        
        CVOpenGLTextureCacheCreateTextureFromImage(kCFAllocatorDefault, textureCache!, cvPixelBuffer!, nil, &cvTexture)
        
        if let cvTexture = cvTexture {
            
            context.makeCurrentContext()
            
            let target = CVOpenGLTextureGetTarget(cvTexture)
            
            glBindTexture(target, CVOpenGLTextureGetName(cvTexture));
            
            // Set up filter and wrap modes for this texture object
            glTexParameteri(target, GLenum(GL_TEXTURE_WRAP_S), GL_CLAMP_TO_EDGE);
            glTexParameteri(target, GLenum(GL_TEXTURE_WRAP_T), GL_CLAMP_TO_EDGE);
            glTexParameteri(target, GLenum(GL_TEXTURE_MAG_FILTER), GL_LINEAR);
            #if ESSENTIAL_GL_PRACTICES_IOS
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            #else
            glTexParameteri(target, GLenum(GL_TEXTURE_MIN_FILTER), GL_LINEAR_MIPMAP_LINEAR);
            #endif
            
            // Allocate a texture image with which we can render to
            // Pass NULL for the data parameter since we don't need to load image data.
            //     We will be generating the image by rendering to this texture
            glTexImage2D(target,
                         0, GL_RGBA,
                         GLsizei(processSize.width), GLsizei(processSize.height),
                         0, GLenum(GL_RGBA),
                         GLenum(GL_UNSIGNED_BYTE), nil);
            
            glGenRenderbuffers(1, &depthBuffer);
            glBindRenderbuffer(GLenum(GL_RENDERBUFFER), depthBuffer);
            glRenderbufferStorage(GLenum(GL_RENDERBUFFER), GLenum(GL_DEPTH_COMPONENT16), GLsizei(processSize.width), GLsizei(processSize.height))
            
            glGenFramebuffers(1, &frameBuffer)
            glBindFramebuffer(GLenum(GL_FRAMEBUFFER), frameBuffer)
            glFramebufferTexture2D(GLenum(GL_FRAMEBUFFER), GLenum(GL_COLOR_ATTACHMENT0), target, CVOpenGLTextureGetName(cvTexture), 0)
            
            glFramebufferRenderbuffer(GLenum(GL_FRAMEBUFFER), GLenum(GL_DEPTH_ATTACHMENT), GLenum(GL_RENDERBUFFER), depthBuffer)
            
            if(glCheckFramebufferStatus(GLenum(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
                NSLog("failed to make complete framebuffer object %x", glCheckFramebufferStatus(GLenum(GL_FRAMEBUFFER)))
            }
            print("cvBuffer create complete")
        }
        
        //context.flushBuffer()
        
    }
    
    func cache(client: SyphonClient, img : SyphonImage) {
    
        //if client.hasNewFrame, let img = client.newFrameImage() {
            
            
            let tex = img.textureName
            var fSize = img.textureSize
            
            if processSize != fSize {
                createCVBuffer(size: (Int(fSize.width),Int(fSize.height)),tclient: client)
            }
            
            
            context.makeCurrentContext()
            
            //context.clearDrawable()
            
            glBindFramebuffer(GLenum(GL_FRAMEBUFFER), frameBuffer)
            glClearColor(1, 0, 0, 1.0);
            glClear(GLbitfield(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
            glViewport(0, 0, GLsizei(fSize.width), GLsizei(fSize.height));
            
            
            glBindTexture(CVOpenGLTextureGetTarget(cvTexture!), tex)
            glBegin(GLenum(GL_QUADS))
            glTexCoord2f(0.0, 0.0)
            glVertex3f(GLfloat(-fSize.width), GLfloat(fSize.height), 0)
            glTexCoord2f(0.0, 1.0)
            glVertex3f(GLfloat(-fSize.width), GLfloat(-fSize.height), 0)
            glTexCoord2f(1.0, 1.0)
            glVertex3f(GLfloat(fSize.width),GLfloat(-fSize.height), 0)
            glTexCoord2f(1.0, 0.0)
            glVertex3f(GLfloat(fSize.width),GLfloat(fSize.height), 0)
            glEnd()
            
            glBindTexture(CVOpenGLTextureGetTarget(cvTexture!), 0)
            
            context.flushBuffer()
            glBindFramebuffer(GLenum(GL_FRAMEBUFFER), 0);
        }
        
    
    
}
