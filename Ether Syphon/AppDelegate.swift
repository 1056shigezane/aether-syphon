//
//  AppDelegate.swift
//  Vapor Syphon
//
//  Created by 稲田成実 on 2018/07/24.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

import Cocoa


let main = DispatchQueue.main

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        // Insert code here to initialize your application
        ProcessInfo().beginActivity(options: .latencyCritical, reason: "Think yourself")
       // NSApplication.shared.
        //NSApplication.shared
        NSApplication.shared.mainWindow?.title = "Aether Syphon"
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }

    
    
}

