//
//  CMsampleMTKView.swift
//  CamSender
//
//  Created by 稲田成実 on 2018/07/02.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

import Foundation
import Metal
import MetalKit
import Aether

public struct NSTVertex {
    var position : vector_float4
    var coordinate : vector_float2
    let dummy : vector_float2 = vector_float2(0,0)
}

class CVTextureCache : NSObject {
    public enum resizeMode : Int {
        case fill = 0
        case fit = 1
        case aspectFill = 2
    }
    var renderPipelineState : MTLRenderPipelineState? = nil
    //var cachedTexture : MTLTexture? = nil
    //var cachedIntermediateTexture : MTLTexture? = nil
    var cachedDepthTexture : MTLTexture? = nil
    var fillVertexData = [NSTVertex(position:[-1,1,0.0,1.0],coordinate:[0.0,0.0]),
                          NSTVertex(position:[1,1,0.0,1.0],coordinate:[1.0,0.0]),
                          NSTVertex(position:[1,-1,0.0,1.0],coordinate:[1.0,1.0]),
                          
                          NSTVertex(position:[-1,1,0.0,1.0],coordinate:[0.0,0.0]),
                          NSTVertex(position:[-1,-1,0.0,1.0],coordinate:[0.0,1.0]),
                          NSTVertex(position:[1,-1,0.0,1.0],coordinate:[1.0,1.0]),
                          ]
    
    var device : MTLDevice!
    var commandQueue : MTLCommandQueue!
    //var library : MTLLibrary!
    
    var pixelBufferPool : CVPixelBufferPool? = nil
    var pixelBuffer : CVPixelBuffer? = nil
    var cvMetalTextureCache : CVMetalTextureCache? = nil
    var cvMetalTexture : CVMetalTexture? = nil
    
    var cvOpenGLTextureCache : CVOpenGLTextureCache? = nil
    var cvOpenGLTexture : CVOpenGLTexture? =  nil
    
    var renderPassDescriptor : MTLRenderPassDescriptor = MTLRenderPassDescriptor()
    
    
    private var textureCache : CVMetalTextureCache? = nil //forCahce
    
    
    init(with ctx:NSOpenGLContext) {
        super.init()
        //cachedTexture = NSTMetalUtils.shared.blancTexture
        
        device = MTLCreateSystemDefaultDevice()
        
        commandQueue = device.makeCommandQueue()
        
        do{
            
            renderPipelineState = try makeRenderPipelineState(vertexFunctionName: "vertex_func", fragmentFunctionName: "aspectCache_func")
            
        }catch{
            print(error)
        }
        
        CVMetalTextureCacheCreate(kCFAllocatorMalloc, nil, device, nil, &textureCache)
        
        
        
        _ = CVMetalTextureCacheCreate(kCFAllocatorMalloc, nil, device, /*[kCVPixelFormatName:kCVPixelFormatType_32BGRA,kCVPixelFormatOpenGLCompatibility: kCFBooleanTrue,kCVPixelBufferMetalCompatibilityKey:kCFBooleanTrue] as CFDictionary*/ nil , &cvMetalTextureCache)
        
        _ = CVOpenGLTextureCacheCreate(kCFAllocatorMalloc, nil, ctx.cglContextObj!, CGLGetPixelFormat(ctx.cglContextObj!)!, nil, &cvOpenGLTextureCache)
        
        
        //print("\(pixelBufferPool != nil), \(cvMetalTextureCache != nil), \(cvOpenGLTextureCache != nil)")
        
    }
    
    deinit {
        print("ÆTexCache Deinit")
        
        textureCache = nil
        
        pixelBufferPool = nil
        pixelBuffer = nil
        
        cvMetalTextureCache = nil
        cvMetalTexture = nil
        
        cvOpenGLTextureCache = nil
        cvOpenGLTexture = nil
        
    }
    
    var mode: resizeMode = .aspectFill
    
    
    func setupTexture(ww:Int,hh:Int){
        
        
        var needsRecreate : Bool = false
        if pixelBuffer != nil {
            
            let cvw = CVPixelBufferGetWidth(pixelBuffer!)
            let cvh = CVPixelBufferGetHeight(pixelBuffer!)
            
            if cvw != ww || cvh != hh {
                needsRecreate = true
            }
            
        }else{
            needsRecreate = true
        }
        
        
        
        if needsRecreate {
            
            
            
            let sourcePixelBufferOptions: NSDictionary = [kCVPixelBufferPixelFormatTypeKey: kCVPixelFormatType_32BGRA,
                                                          kCVPixelBufferWidthKey: Int32(ww),
                                                          kCVPixelBufferHeightKey: Int32(hh),
                                                          kCVPixelFormatOpenGLCompatibility: kCFBooleanTrue!,
                                                          kCVPixelBufferMetalCompatibilityKey:kCFBooleanTrue!,
                //kCVPixelBufferIOSurfacePropertiesKey:NSDictionary()
            ]
            
            pixelBufferPool = nil
            CVPixelBufferPoolCreate(kCFAllocatorMalloc, nil, sourcePixelBufferOptions, &pixelBufferPool)
            
            
            pixelBuffer = nil
            
            CVPixelBufferPoolCreatePixelBuffer(kCFAllocatorMalloc, pixelBufferPool!, &pixelBuffer)
            
            cvMetalTexture = nil
            cvOpenGLTexture = nil
            
            CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorMalloc, cvMetalTextureCache!, pixelBuffer!, nil, .bgra8Unorm, ww, hh, 0, &cvMetalTexture)
            CVOpenGLTextureCacheCreateTextureFromImage(kCFAllocatorMalloc, cvOpenGLTextureCache!, pixelBuffer!, nil, &cvOpenGLTexture)
            
            
            renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0, 0, 0, 1)
            renderPassDescriptor.colorAttachments[0].loadAction = .clear
            renderPassDescriptor.colorAttachments[0].storeAction = .store
            renderPassDescriptor.colorAttachments[0].texture = CVMetalTextureGetTexture(cvMetalTexture!)!
        }
        
        
        
        
    }
    
    func cacheToTexture(pixelBuffer pix:CVPixelBuffer,width:Int,height:Int, affineTransform:CGAffineTransform? = nil) {
        
        let count = CVPixelBufferGetPlaneCount(pix)
        
        if count == 0 {
            setupTexture(ww: width, hh: height)
            var texture: CVMetalTexture? = nil
           let status = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorMalloc, textureCache!, pix, nil, .bgra8Unorm, width, height, 0, &texture)
            
            if let tex = CVMetalTextureGetTexture(texture!) {
            
            //print("Non Planer TEx:",tex)
            let commandBuffer = commandQueue.makeCommandBuffer()
            AspectRendering.render(to: renderPassDescriptor, texture: tex, mode: .fill, commandBuffer: commandBuffer)
            commandBuffer?.commit()
                
            }
        }else
        if (count > 1) {
        
        let w = width
        let h = height
        var ww : Int = w
        var hh : Int = h
        
        if let af = affineTransform {
            ww = abs(Int(CGFloat(w) * af.a + CGFloat(h) * af.b))
            hh = abs(Int(CGFloat(w) * af.c + CGFloat(h) * af.d))
        }
        
        
        
        let vasp = Float(width) / Float(height)
        let tasp = Float(ww) / Float(hh)
        
        let asp : Float = vasp / tasp
        
        
        setupTexture(ww: ww, hh: hh)
        
        
        
        
        
            
            //CVPixelBufferLockBaseAddress(pix, [.readOnly])
            CVPixelBufferLockBaseAddress(pix, .readOnly)
            let imageTextureY = createTexture(fromPixelBuffer: pix, pixelFormat:.r8Unorm, planeIndex:0)!
            let imageTextureCbCr = createTexture(fromPixelBuffer: pix, pixelFormat:.rg8Unorm, planeIndex:1)!
            CVPixelBufferUnlockBaseAddress(pix, .readOnly)
            
            
            var scale : vector_float2 = vector_float2(1.0,1.0)
            switch mode {
            case .fill:
                
                break
            case .fit:
                if asp < 1 {
                    scale.x /= asp
                }else{
                    scale.y *= asp
                }
                break
            case .aspectFill:
                if (asp < 1) {
                    scale.y *= asp;
                }else{
                    scale.x /= asp;
                }
                break
            }
            
            
            let commandBuffer = commandQueue.makeCommandBuffer()
            let rce = commandBuffer?.makeRenderCommandEncoder(descriptor: renderPassDescriptor)
            rce?.label = "TextureCache:rce"
            
            
            
            
            
            rce?.setRenderPipelineState(renderPipelineState!)
            rce?.setVertexBytes(&fillVertexData, length: MemoryLayout<NSTVertex>.size * fillVertexData.count, index: 0)
            
            rce?.setFragmentBytes(&scale, length: MemoryLayout<vector_float2>.size, index: 0)
            rce?.setFragmentTexture(imageTextureY, index: 0)
            rce?.setFragmentTexture(imageTextureCbCr, index: 1)
            
            
            rce?.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: fillVertexData.count)
            rce?.endEncoding()
            
            
            
            commandBuffer?.commit()
            
            
            
        }else{
            setupTexture(ww: width, hh: height)
            let commandBuffer = commandQueue.makeCommandBuffer()
            AspectRendering.render(to: renderPassDescriptor, pix: pix, mode: .fill, commandBuffer: commandBuffer)
            commandBuffer?.commit()
        }
        
        
    }
    
    
    
}

extension CVTextureCache {
    public func makeRenderPipelineState(vertexFunctionName:String,fragmentFunctionName:String,alphaBlend:Bool = false) throws -> MTLRenderPipelineState? {
        if let library = device.makeDefaultLibrary() {
        let vFunc = library.makeFunction(name: vertexFunctionName)
        vFunc?.label = vertexFunctionName
        let fFunc = library.makeFunction(name: fragmentFunctionName)
        fFunc?.label = fragmentFunctionName
        let vertexDescriptor = MTLVertexDescriptor()
        vertexDescriptor.attributes[0].format = .float4;
        vertexDescriptor.attributes[0].offset = 0;
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.layouts[0].stepFunction = .perPatchControlPoint;
        vertexDescriptor.layouts[0].stepRate = 1;
        vertexDescriptor.layouts[0].stride = 4*MemoryLayout<Float>.size;
        
        let rpd = MTLRenderPipelineDescriptor()
        rpd.label = vertexFunctionName + " & " + fragmentFunctionName
        rpd.vertexFunction = vFunc
        rpd.fragmentFunction = fFunc
        rpd.colorAttachments[0].pixelFormat = .bgra8Unorm
        rpd.depthAttachmentPixelFormat = .invalid
        rpd.vertexDescriptor = vertexDescriptor
        
        if alphaBlend {
            rpd.isAlphaToCoverageEnabled = true
            rpd.colorAttachments[0].isBlendingEnabled = true
            rpd.colorAttachments[0].rgbBlendOperation = .add
            rpd.colorAttachments[0].alphaBlendOperation = .add
            rpd.colorAttachments[0].sourceRGBBlendFactor = .blendColor
            rpd.colorAttachments[0].sourceAlphaBlendFactor = .blendAlpha
            rpd.colorAttachments[0].destinationRGBBlendFactor = .oneMinusBlendColor
            rpd.colorAttachments[0].destinationAlphaBlendFactor = .oneMinusBlendAlpha
            }
            
            do{
                let rps = try device.makeRenderPipelineState(descriptor: rpd)
                return rps
            }catch let error {
                throw error
            }
        }else{
            throw NSError()
            
        }
    }
    
    public func simpleBlit(from:MTLTexture,to:MTLTexture,commandBuffer:MTLCommandBuffer?){
        
        let blit = commandBuffer?.makeBlitCommandEncoder()
        blit?.label = "simpleBlit"
        blit?.copy(from: from, sourceSlice: 0, sourceLevel: 0, sourceOrigin: MTLOriginMake(0, 0, 0), sourceSize: MTLSizeMake(from.width, from.height, from.depth), to: to, destinationSlice: 0, destinationLevel: 0, destinationOrigin: MTLOriginMake(0, 0, 0))
        blit?.endEncoding()
    }
    
    
    public func createTexture(fromPixelBuffer pixelBuffer: CVPixelBuffer, pixelFormat: MTLPixelFormat, planeIndex: Int) -> MTLTexture? {
        var mtlTexture: MTLTexture? = nil
        
        var texture: CVMetalTexture? = nil
        #if os(macOS)
        let status = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorMalloc, textureCache!, pixelBuffer, nil, pixelFormat, CVPixelBufferGetWidthOfPlane(pixelBuffer, planeIndex), CVPixelBufferGetHeightOfPlane(pixelBuffer, planeIndex), planeIndex, &texture)
        #elseif os(iOS)
        let status = CVMetalTextureCacheCreateTextureFromImage(nil, textureCache!, pixelBuffer, nil, pixelFormat, CVPixelBufferGetWidthOfPlane(pixelBuffer, planeIndex), CVPixelBufferGetHeightOfPlane(pixelBuffer, planeIndex), planeIndex, &texture)
        #endif
        if status == kCVReturnSuccess {
            mtlTexture = CVMetalTextureGetTexture(texture!)
        }
        
        return mtlTexture
    }
    
}
