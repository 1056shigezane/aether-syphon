//
//  SenderView.swift
//  Vapor Syphon
//
//  Created by 稲田成実 on 2018/07/24.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

import Foundation
import Cocoa
import Aether
import CoreMedia
import GLUT

var threadIsMain  : Bool{
    return Thread.current == Thread.main
}

func pThreadIsMain() {
    print("Thread is Main?", threadIsMain)
}

class VapeurSyphonServer : NSObject {
    
    var appName : String = ""
    var name : String = ""
    var UUID : String = ""
    var syphonServerDescription : [String:Any]!
    
    var client : SyphonClient? = nil
    
    
    var previewWindow : NSWindow!
    var previewController : PreviewController!
    
    
    private var vaporServer : VideoServer? = nil
    private var ltime : Double = 0.0
    
    
    var averageBitrate : Int = 2000000 {
        didSet{
            vaporServer?.averageBitrate = averageBitrate
        }
        
    }
    
    var publishing : Bool = false {
        didSet {
            if publishing != oldValue {
                if publishing {
                    
                    if vaporServer == nil {
                        vaporServer = VideoServer(name: appName + "_" + name)
                        vaporServer?.averageBitrate = averageBitrate
                    }
                    
                }else{
                    
                    
                    if vaporServer != nil {
                        vaporServer!.stop()
                    }
                    vaporServer = nil
                    
                    
                    previewWindow.close()
                }
            }
        }
    }
    
    public func stop()  {
        publishing = false
        if vaporServer != nil {
            vaporServer?.stop()
            
        }
        
        if client != nil {
            client?.stop()
            client = nil
        }
        
        
        vaporServer = nil
        let p = self.previewWindow
        DispatchQueue.main.async {
            p?.close()
        }
        
        
    }
    
    deinit {
        print("VapeurSyphonServer Deinit")
        stop()
        previewWindow.contentViewController = nil
        previewController = nil
    }
    
    
    init(with serverDescription: [String:Any]) {
        super.init()
        
        previewController = (NSStoryboard.init(name: "Main", bundle: Bundle.main).instantiateController(withIdentifier: "PreviewController") as! PreviewController)
        
        previewController.loadView()
        previewController.viewDidLoad()
        
        previewWindow = NSWindow(contentViewController: previewController)
        
        
        
        syphonServerDescription = serverDescription
        appName = serverDescription[SyphonServerDescriptionAppNameKey] as! String
        name = serverDescription[SyphonServerDescriptionNameKey] as! String
        UUID = serverDescription[SyphonServerDescriptionUUIDKey] as! String
        
        previewWindow.title = "Server: - " + appName + "/" + name
        previewWindow.titlebarAppearsTransparent = true
        previewWindow.styleMask = NSWindow.StyleMask(rawValue: previewWindow.styleMask.rawValue | NSWindow.StyleMask.fullSizeContentView.rawValue)
        
        //print("iodesc:\(syphonServerDescription)")
        
        if let context = previewController.syphonView.openGLContext?.cglContextObj {
            
            client = SyphonClient(serverDescription: serverDescription, context: context, options: nil, newFrameHandler: {
                [weak self] tclient in
                
                //RunLoop.main.perform(inModes:[.default,.common]) {
                    
                main.async{
                    if tclient != nil {
                        self?.newFrameHandler(tclient: tclient!)
                    }
                }
            })
            
            
        }
        ltime = Date.timeIntervalSinceReferenceDate
        
    }
    
    
    
    func newFrameHandler(tclient:SyphonClient){
      //  main.async{
        let time = Date.timeIntervalSinceReferenceDate
        //let dur = time - ltime
        //print(time)
            self.ltime = time
        
            self.previewController.syphonView.image = tclient.newFrameImage()
        
            if self.previewController.syphonView.image != nil {
            
                self.previewController.syphonView.cache(needsCache: self.publishing, needsDraw: self.previewController.isPresenting)
            
                if self.publishing , self.previewController.syphonView.cvPixelBuffer != nil {
                
                    self.vaporServer?.publish(pixelBuffer: self.previewController.syphonView.cvPixelBuffer!, pts: time.cmTime)
                
            }
            
        }
       // }
    }
    
    func preview(){
        if client != nil {
            previewWindow.orderFrontRegardless()
        }
    }
    
    
}

class ServerCellDataBase : NSObject {
    
    weak var tableView : NSTableView? = nil
    class var shared : ServerCellDataBase {
        struct Shared {
            static var instance : ServerCellDataBase = ServerCellDataBase()
        }
        return Shared.instance
    }
    
    var servers : [String : VapeurSyphonServer] = [:]
    
    override init() {
        super.init()
        reflesh()
        SyphonServerDirectory.shared().addObserver(self, forKeyPath: #keyPath(SyphonServerDirectory.servers), options: .new, context: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == #keyPath(SyphonServerDirectory.servers) {
            
            
            reflesh()
            
            
        }
    }
    
    func reflesh(){
        
        let descs = SyphonServerDirectory.shared()?.servers as! [[String:Any]]
        
        for desc in descs {
            let uuid = desc[SyphonServerDescriptionUUIDKey] as! String
            
            if servers.index(forKey: uuid) == nil { // key has empty
                servers[uuid] = VapeurSyphonServer(with: desc)
            }
        }
        
        for key in servers.keys {
            if let vs = servers[key]{
                
                if !(SyphonServerDirectory.shared()?.servers.contains(where: {
                    ( v : Any ) in
                    
                    if let desc = v as? [String:Any] {
                        let uuid1 = desc[SyphonServerDescriptionUUIDKey] as! String
                        let uuid2 = vs.syphonServerDescription[SyphonServerDescriptionUUIDKey] as! String
                        return uuid1 == uuid2
                        
                    }else{
                        return false
                    }
                    
                }))! {//!contains
                    servers[key]?.stop()
                    
                    
                    
                    
                    
                    servers.removeValue(forKey: key)
                    
                }
                
                
            }
        }
        
        tableView?.reloadData()
    }
    
    
}

class ServerCellView : NSView {
    
    @IBOutlet weak var appNameLabel: NSTextField!
    
    @IBOutlet weak var nameLabel: NSTextField!
    
    @IBOutlet weak var bitrateLabel: NSTextField!
    
    
    
    @IBOutlet weak var bitrateSlider: NSSlider!
    
    @IBAction func bitrateSlider(_ sender: NSSlider) {
        let v = sender.floatValue
        let rate = rateFromNormalisedValue(v)//Int(192000 + (50000000 - 192000) * v)
        vapeurSyphonServer.averageBitrate = rate
        
        ///let bl = rate > 1000000 ? "\(rate / 1000000)Mbps" : "\(rate / 1000)Kbps"
        bitrateLabel.stringValue = rate > 1000000 ? "\(rate / 1000000)Mbps" : "\(rate / 1000)Kbps"
    }
    
    func rateFromNormalisedValue(_ v:Float) -> Int {
        let pow4 = pow(v * 2.0 ,4.0)
        if pow4 < 1.0 {
            return Int(pow4 * (2000000 - 192000) + 192000)
        }else{
            return Int(( pow4 - 1 ) / 15.0 * (50000000 - 2000000) + 2000000)
        }
        
    }
    func sliderNormalizedValueFromBitrate(_ v:Int) -> Float{
        if v > 2000000 {
            return pow((Float(v) - 2000000) / (50000000 - 2000000) * 15 + 1, 0.25) / 2
        }else{
            return pow( (Float(v) - 192000) / (2000000 - 192000), 0.25 ) / 2
        }
        
       // return Float(v - 192000) / (50000000 - 192000)
    }
    
    @IBOutlet weak var publishToggle: NSButton!
    
    @IBAction func publishToggleChange(_ sender: NSButton) {
        
        vapeurSyphonServer.publishing = sender.state.rawValue == 1
        
        
    }
    
    @IBAction func previewButtonPressed(_ sender: NSButton) {
        
        vapeurSyphonServer.preview()
        
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
    
    deinit {
        print("ServerCellView deinit")
    }
    
    weak var vapeurSyphonServer : VapeurSyphonServer! {
        didSet{
            appNameLabel.stringValue = vapeurSyphonServer.appName
            nameLabel.stringValue = vapeurSyphonServer.name
            publishToggle.state = NSControl.StateValue.init(vapeurSyphonServer!.publishing ? 1 : 0)
            
            if let rate = vapeurSyphonServer?.averageBitrate {
            bitrateLabel.stringValue = rate > 1000000 ? "\(rate / 1000000)Mbps" : "\(rate / 1000)Kbps"
                bitrateSlider.floatValue = sliderNormalizedValueFromBitrate(rate)
            }
        }
    }
    
    
    
    
}


class SenderViewController : NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    

    @IBOutlet weak var tableView: NSTableView!
    
    
    
    var syphonServerDirectory : SyphonServerDirectory { return SyphonServerDirectory.shared() }
    
    
    
    
    override func viewDidLoad() {
        ServerCellDataBase.shared.tableView = self.tableView
        
        
    }
    override func viewWillAppear() {
        
    }
    
    override func viewDidAppear() {
        tableView.sizeLastColumnToFit()
    }
    
    
    func tableView(_ tableView: NSTableView, sizeToFitWidthOfColumn column: Int) -> CGFloat {
        return tableView.frame.size.width
    }
    
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        
       return ServerCellDataBase.shared.servers.count
        
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        let view = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ServerCellView"), owner: self) as! ServerCellView
        //view.serverDescription = serverDesc
        var idx = ServerCellDataBase.shared.servers.startIndex
        for _ in 0 ..< row {
            idx = ServerCellDataBase.shared.servers.index(after: idx)
        }
        
        view.vapeurSyphonServer = ServerCellDataBase.shared.servers[idx].value
        
        
        
        return view
    }
    
    
    
    
    
}


class PreviewController : NSViewController {
    
    @IBOutlet weak var syphonView: SyphonView!
    
    var isPresenting : Bool = false
    
    override func viewDidAppear() {
        isPresenting = true
    }
    
    override func viewDidDisappear() {
        isPresenting = false
    }
    
    deinit{
        print("PreviewController deinit")
    }
    
}




class SyphonView : NSOpenGLView {
    weak var image : SyphonImage!
    var texture : GLuint {
        get{
            return image.textureName
        }
    }
    
    var textureCache : CVOpenGLTextureCache? = nil
    var cvPixelBufferPool : CVPixelBufferPool? = nil
    var cvPixelBuffer : CVPixelBuffer? = nil
    var cvTexture : CVOpenGLTexture? = nil
    
    //var depthBuffer : GLuint = 0
    var frameBuffer : GLuint = 0
    
    var processSize : NSSize = NSSize.zero
    
    deinit {
        print("SyphonView deinit")
        textureCache = nil
        cvPixelBufferPool = nil
        cvPixelBuffer = nil
        cvTexture = nil
        
        glDeleteFramebuffers(1, &frameBuffer)
        CGLDestroyContext((openGLContext?.cglContextObj)!)
    }
    
    
    func createCVBuffer(size:(width:Int, height:Int)) {
        
        if size.width <= 0 || size.height <= 0 {
            return
        }
        
        guard let cglctx = openGLContext?.cglContextObj else{
            print("cglCtx is nil")
            return
        }
        
        guard let fmt = CGLGetPixelFormat(cglctx) else{
            return
        }
        
        
        _ = CVOpenGLTextureCacheCreate(kCFAllocatorMalloc, [kCVOpenGLTextureCacheChromaSamplingModeKey:kCVOpenGLTextureCacheChromaSamplingModeBestPerformance] as CFDictionary, cglctx, fmt, nil, &textureCache)
        
        #if os(iOS)
        let sourcePixelBufferOptions: NSDictionary = [kCVPixelBufferPixelFormatTypeKey: kCVPixelFormatType_32BGRA,
                                                      kCVPixelBufferWidthKey: Int32(size.width),
                                                      kCVPixelBufferHeightKey: Int32(size.height),
                                                      kCVPixelFormatOpenGLESCompatibility: kCFBooleanTrue,
                                                      kCVPixelBufferMetalCompatibilityKey: kCFBooleanTrue,
                                                      kCVPixelBufferIOSurfacePropertiesKey: NSDictionary()]
        #elseif os(macOS)
        let sourcePixelBufferOptions: NSDictionary = [kCVPixelBufferPixelFormatTypeKey: kCVPixelFormatType_32BGRA,
                                                      kCVPixelBufferWidthKey: Int32(size.width),
                                                      kCVPixelBufferHeightKey: Int32(size.height),
                                                      kCVPixelFormatOpenGLCompatibility: kCFBooleanTrue,
                                                      kCVPixelBufferMetalCompatibilityKey: kCFBooleanTrue,
                                                      //kCVPixelBufferIOSurfacePropertiesKey: NSDictionary()
                                                        ]
        #endif
        
        
        CVPixelBufferPoolCreate(kCFAllocatorMalloc, nil, sourcePixelBufferOptions, &cvPixelBufferPool)
        CVPixelBufferPoolCreatePixelBuffer(kCFAllocatorMalloc, cvPixelBufferPool!, &cvPixelBuffer)
        
        print("CVPBPAKs:\(CVPixelBufferPoolGetAttributes(cvPixelBufferPool!))")
        
        guard textureCache != nil, cvPixelBuffer != nil else{
            print("textureCache, cvPixelBuffer is nil")
            return
        }
        
        
        
        
        CVOpenGLTextureCacheCreateTextureFromImage(kCFAllocatorMalloc, textureCache!, cvPixelBuffer!, nil, &cvTexture)
        
        if cvTexture != nil {
            
            processSize = NSSize(width: size.width, height: size.height)
            
            glEnable(GLenum(GL_FRAMEBUFFER))
            glEnable(GLenum(GL_TEXTURE_RECTANGLE_EXT))
            
            let target = CVOpenGLTextureGetTarget(cvTexture!)
            
            //glBindTexture(target, CVOpenGLTextureGetName(cvTexture!));
            
            // Set up filter and wrap modes for this texture object
            /*glTexParameteri(target, GLenum(GL_TEXTURE_WRAP_S), GL_CLAMP_TO_EDGE);
            glTexParameteri(target, GLenum(GL_TEXTURE_WRAP_T), GL_CLAMP_TO_EDGE);
            glTexParameteri(target, GLenum(GL_TEXTURE_MAG_FILTER), GL_NEAREST);
            glTexParameteri(target, GLenum(GL_TEXTURE_MIN_FILTER), GL_LINEAR_MIPMAP_LINEAR);*/
            /*#if ESSENTIAL_GL_PRACTICES_IOS
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            #else
            glTexParameteri(target, GLenum(GL_TEXTURE_MIN_FILTER), GL_LINEAR_MIPMAP_LINEAR);
            #endif
            */
            // Allocate a texture image with which we can render to
            // Pass NULL for the data parameter since we don't need to load image data.
            //     We will be generating the image by rendering to this texture
            /*glTexImage2D(target,
                         0, GL_RGBA,
                         GLsizei(processSize.width), GLsizei(processSize.height),
                         0, GLenum(GL_RGBA),
                         GLenum(GL_UNSIGNED_BYTE), nil);
            */
            /*
            glGenRenderbuffers(1, &depthBuffer);
            glBindRenderbuffer(GLenum(GL_RENDERBUFFER), depthBuffer);
            glRenderbufferStorage(GLenum(GL_RENDERBUFFER), GLenum(GL_DEPTH_COMPONENT16), GLsizei(processSize.width), GLsizei(processSize.height))
            */
            
            glGenFramebuffers(1, &frameBuffer)
            glBindFramebuffer(GLenum(GL_FRAMEBUFFER), frameBuffer)
            glFramebufferTexture2D(GLenum(GL_FRAMEBUFFER), GLenum(GL_COLOR_ATTACHMENT0), target, CVOpenGLTextureGetName(cvTexture!), 0)
            
            //glFramebufferRenderbuffer(GLenum(GL_FRAMEBUFFER), GLenum(GL_DEPTH_ATTACHMENT), GLenum(GL_RENDERBUFFER), depthBuffer)
            
            if(glCheckFramebufferStatus(GLenum(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
                NSLog("failed to make complete framebuffer object %x", glCheckFramebufferStatus(GLenum(GL_FRAMEBUFFER)))
            }
            
            
            //glBindRenderbuffer(GLenum(GL_RENDERBUFFER), 0)
            glBindFramebuffer(GLenum(GL_FRAMEBUFFER), 0)
            //glBindTexture(target, 0);
            
            print("cvBuffer create complete")
        }
        
        //context.flushBuffer()
        
    }
    
    let minimumWidth:CGFloat = 640
    
    public func cache(needsCache:Bool, needsDraw:Bool){
      //  print("Sender Cache Thread is Main:", Thread.current == .main)
        guard let ctx = openGLContext else{return}
        ctx.makeCurrentContext()
        if needsCache || needsDraw {
            glMatrixMode(GLenum(GL_PROJECTION))
            glLoadIdentity()
            glOrtho(0.0, 1.0, 0.0, 1.0, -1, 1)
            
            glMatrixMode(GLenum(GL_MODELVIEW))
            glLoadIdentity()
        }
        
        if needsCache {
            //print("cache")
            cache()
            glFlush()
        }
        
        
        if needsDraw {
            
            //print("draw")
            draw(frame)
            glFlush()
        }
        
      /*  if needsCache || needsDraw {
            glFlush()
        }*/
        
    }
    
    private func cache(){
        
        var textureSize = image.textureSize
        
        /*if textureSize.width < minimumWidth || textureSize.height < minimumWidth {
            let asp = textureSize.width / textureSize.height
            
            if asp < 1.0 {
                textureSize.width = minimumWidth
                textureSize.height = textureSize.width / asp
            }else{
                textureSize.height = minimumWidth
                textureSize.width = textureSize.height * asp
            }
        }*/
        
        textureSize.width = round( textureSize.width / 16.0 ) * 16.0
        textureSize.height = round( textureSize.height / 16.0 ) * 16.0
        
        if processSize != textureSize {
            
            createCVBuffer(size: (Int(textureSize.width),Int(textureSize.height)))
        }
        
        let tex_coords : [GLfloat] =
            [
                0.0,                0.0,
                Float(image.textureSize.width),  0.0,
                Float(image.textureSize.width),  Float(image.textureSize.height),
                0.0,                Float(image.textureSize.height)
        ]
        
        
        let verts : [GLfloat] =
            [
                0, 1,
                1, 1,
                1, 0,
                0, 0
        ]
        
        glViewport(0, 0, GLsizei(textureSize.width), GLsizei(textureSize.height));
        
        
        
        //glClearColor(0.0, 0.0, 0.0, 1.0);
        //glClear(GLbitfield(GL_COLOR_BUFFER_BIT));
        
        glEnable(GLenum(GL_FRAMEBUFFER))
        
        glBindFramebuffer(GLenum(GL_FRAMEBUFFER), frameBuffer)
        
        glEnable(GLenum(GL_TEXTURE_RECTANGLE_EXT))
        
        
        
        glBindTexture(GLenum(GL_TEXTURE_RECTANGLE_EXT), image.textureName)
        
        glColor4f(1.0, 1.0, 1.0, 1.0)
        
        
        glEnableClientState( GLenum(GL_TEXTURE_COORD_ARRAY) )
        glTexCoordPointer(2, GLenum(GL_FLOAT), 0, tex_coords )
        glEnableClientState(GLenum(GL_VERTEX_ARRAY))
        glVertexPointer(2, GLenum(GL_FLOAT), 0, verts )
        
        glDrawArrays( GLenum(GL_TRIANGLE_FAN), 0, 4 )
        glDisableClientState( GLenum(GL_TEXTURE_COORD_ARRAY) )
        glDisableClientState(GLenum(GL_VERTEX_ARRAY))
        
        glBindTexture(GLenum(GL_TEXTURE_RECTANGLE_EXT), 0)
        glDisable(GLenum(GL_TEXTURE_RECTANGLE_EXT))
        
        glBindFramebuffer(GLenum(GL_FRAMEBUFFER), 0)
        glDisable(GLenum(GL_FRAMEBUFFER))
        
        
        
        
        
    }
    
    
    
    override func draw(_ dirtyRect: NSRect) {
        
        //CGLContextObj cgl_ctx = [[self openGLContext] CGLContextObj];
        autoreleasepool {
            if image != nil {
                openGLContext?.makeCurrentContext()
                
                
                let textureSize = image.textureSize
                
                
                let tasp = textureSize.width / textureSize.height
                let vasp = frame.size.width / frame.size.height
                
                let asp : Float = Float(tasp / vasp)
                var x : Float = 1.0
                var y : Float = 1.0
                if asp < 1.0 {
                    x *= asp
                }else{
                    y /= asp
                }
                
                
                
                
                let tex_coords : [GLfloat] =
                    [
                        0.0,                0.0,
                        Float(textureSize.width),  0.0,
                        Float(textureSize.width),  Float(textureSize.height),
                        0.0,                Float(textureSize.height)
                ]
                
                
                let verts : [GLfloat] =
                    [
                        0, 0,
                        1, 0,
                        1, 1,
                        0, 1
                ]
                
                
                glViewport(0, 0, GLsizei(frame.size.width * 2), GLsizei(frame.size.height * 2));
                
                /*
                 glMatrixMode(GLenum(GL_PROJECTION));
                 glLoadIdentity();
                 glOrtho(0.0, 1.0, 0.0, 1.0, -1, 1);
                 
                 glMatrixMode(GLenum(GL_MODELVIEW));
                 glLoadIdentity();
                 */
                
                
                glClearColor(0.0, 0.0, 0.0, 1.0);
                glClear(GLbitfield(GL_COLOR_BUFFER_BIT));
                
                glEnable(GLenum(GL_TEXTURE_RECTANGLE_EXT))
                
                
                
                glBindTexture(GLenum(GL_TEXTURE_RECTANGLE_EXT), image.textureName)
                
                
                
                glColor4f(1.0, 1.0, 1.0, 1.0);
                
                
                glEnableClientState( GLenum(GL_TEXTURE_COORD_ARRAY) )
                glTexCoordPointer(2, GLenum(GL_FLOAT), 0, tex_coords )
                glEnableClientState(GLenum(GL_VERTEX_ARRAY))
                glVertexPointer(2, GLenum(GL_FLOAT), 0, verts )
                glDrawArrays( GLenum(GL_TRIANGLE_FAN), 0, 4 )
                glDisableClientState( GLenum(GL_TEXTURE_COORD_ARRAY) )
                glDisableClientState(GLenum(GL_VERTEX_ARRAY))
                
                
                
                glBindTexture(GLenum(GL_TEXTURE_RECTANGLE_EXT), 0)
                glDisable(GLenum(GL_TEXTURE_RECTANGLE_EXT))
                
                
                //[self drawScene];
                
                
                
                //[[self openGLContext] flushBuffer];
                //glFlush();
                //openGLContext?.flushBuffer()
                
                //glViewport(0, 0, GLsizei(frameSize.width), GLsizei(frameSize.height))
                
            }
        }
    }
    
    
}
