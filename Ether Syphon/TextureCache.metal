//
//  TextureCache.metal
//  Vapor Syphon
//
//  Created by 稲田成実 on 2018/07/29.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;
constant float pi = 3.141592653589793;
static constant float4x4 ycbcrToRGBTransform = float4x4(
                                                        float4(+1.0000f, +1.0000f, +1.0000f, +0.0000f),
                                                        float4(+0.0000f, -0.3441f, +1.7720f, +0.0000f),
                                                        float4(+1.4020f, -0.7141f, +0.0000f, +0.0000f),
                                                        float4(-0.7010f, +0.5291f, -0.8860f, +1.0000f)
                                                        );



struct VertexIn {
    float4 position [[attribute(0)]];
    float4 coordinate [[attribute(0)]];
};

struct VertexOut {
    float4 position [[position]];
    float2 coordinate;
};


vertex VertexOut
vertex_func(constant VertexIn * vertices [[buffer(0)]], uint vid[[vertex_id]]){
    VertexIn inVertex = vertices[vid];
    VertexOut outVertex;
    outVertex.position = inVertex.position;
    outVertex.coordinate = float2(inVertex.coordinate[0],inVertex.coordinate[1]);
    return outVertex;
}

fragment float4 aspectCache_func(VertexOut vert [[stage_in]],
                                 texture2d<float, access::sample> capturedImageTextureY [[ texture(0) ]],
                                 texture2d<float, access::sample> capturedImageTextureCbCr [[ texture(1) ]],
                                 constant float2 &scale [[buffer(0)]]
                                 ) {
    constexpr sampler sp(address::clamp_to_zero);
    float2 uv = vert.coordinate - 0.5;
    uv *= scale;
    uv += 0.5;
    
    
    if (uv.x < 0.0 || uv.x > 1.0 || uv.y < 0.0 || uv.y > 1.0) {
        return float4(0.0,0.0,0.0,1.0);
    }else{
        float4 ycbcr = ycbcrToRGBTransform * float4(capturedImageTextureY.sample(sp, uv).r,
                                                    capturedImageTextureCbCr.sample(sp, uv).rg, 1.0);
        return ycbcr;
    }
    
}
